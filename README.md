# Ansible

This project organizes files for **automate resource provisioning, configuration management, and application deployment** in **Ansible** as IaC (Infrastructure as Code) tool:

- **Oracle Cloud (OCI)**

**Author**
- kaylestival@gmail.com
- [linkedin.com/in/kaylestival](linkedin.com/in/kaylestival)
